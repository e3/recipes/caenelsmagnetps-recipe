# caenelsmagnetps conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/caenelsmagnetps"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS Device Support for CAENels family of magnet power supplies
